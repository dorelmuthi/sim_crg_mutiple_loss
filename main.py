import numpy as np
import math
import matplotlib.pyplot as plt
import pickle as pk
from multiprocessing import Pool
import time

from utils import *

LINEAR = 0
EXPONENTIAL = 1
STEPLIKE = 2
ZERO = -1

RANDOM_CONTRIBUTION = -1

NO_ROUND = -1
RANDOM_ROUND = -2
EVERY_ROUND = -3

class Player():
    """
    The players do not know the structure of the game, they follow a
    certain predefined behavior, influenced only by the cumulated total
    contribution. This class allows to save the predefined behaviour of
    each player (i.e. the strategy for each round), as well as the
    player's state for each round they play.
    """

    def __init__(self, wealth=0, lossProbability=None, riskLambda=1, lossFraction=0, totalWealth=0):
        """
        lossProbability should be a function with one parameter returning the
        corresponding loss probability to some cumulative total contribution
        """
        self.threshold = list()
        self.afterThreshold = list()
        self.beforeThreashold = list()
        self.contribution = list()
        self.intialWealth = wealth
        self.currentWealth = wealth
        self.lossFraction = lossFraction
        self.lossProbability = lossProbability
        self.riskLambda = riskLambda
        self.totalWealth = totalWealth

    def addStrategies(self, strategies):
        """
        Saves the player's strategies for each round from a list of strategies.
        """
        for strategy in strategies:
            self.addStrategy(strategy)

    def addStrategy(self, strategy, round=None):
        """
        The function parameter <round> is a triplet, containing a strategy which
        should be followed by the player in a certain round. If the round number
        is not specified, the strategy is linked to the next round number which does
        not have a strategy defined yet.
        """
        if round is None:
            self.threshold.append(strategy[0])
            self.afterThreshold.append(strategy[1])
            self.beforeThreashold.append(strategy[2])
        else:
            # create the missing strategies with pseudo values (0) if their
            # are not created yet
            if round >= len(self.threshold):  # threshold, afterThreshold and beforeThreshold have the same length
                # round numbers begin with 0, hence we need to add 1
                self.threshold.extend([None for i in range(round - len(self.threshold) + 1)])
                self.afterThreshold.extend([None for i in range(round - len(self.afterThreshold) + 1)])
                self.beforeThreashold.extend([None for i in range(round - len(self.beforeThreashold) + 1)])
            self.threshold[round] = strategy[0]
            self.afterThreshold[round] = strategy[1]
            self.beforeThreashold[round] = strategy[2]

    def addContribution(self, contribution, round):
        """
        Saves the value of the contribution the player made during a
        certain round.
        """
        if round >= len(self.contribution):
            self.contribution.extend([None for i in range(round - len(self.contribution) + 1)])
        self.contribution[round] = contribution

    def getContribution(self, round):
        """
        Returns the contribution the player made in a certain round or
        None if the player didn't participate in the respective round.
        """
        if 0 <= round < len(self.contribution):
            return self.contribution[round]
        else:
            return None

    def registerContribution(self, contribution):
        """
        Subtract from the current wealth the contribution the player made.
        """
        self.currentWealth -= contribution

    def registerLoss(self, totalContribution, currentRound, riskEventTime):
        """
        Subtracts a given fraction of the player wealth from its remaining wealth
        if the player lost.
        """
        if (riskEventTime == currentRound or riskEventTime == EVERY_ROUND):
            #print("RiskEventTime:", riskEventTime, "Current Round:", currentRound)
            if np.random.uniform(0., 1.) < self.getLossProbability(totalContribution) :
                self.currentWealth *= (1 - self.lossFraction)

    def getLossProbability(self, totalContribution):
        """
        Returns the loss probability of a player, given the total cumulative contribution.
        """
        return self.lossProbability(totalContribution, self.riskLambda, self.totalWealth)

    def computeContribution(self, totalContribution, round):
        """
        Calculate the amount of the next contribution based on the strategy, the
        already collected cumulative total contribution and the round.
        """
        if totalContribution <= self.threshold[round]:
            return min(self.afterThreshold[round], self.currentWealth)
        else:
            return min(self.beforeThreashold[round], self.currentWealth)

class Game():
    def __init__(self, roundsNb, strategies, wealths, lossFracts, riskEventTime, riskCurve, riskLambda):
        """
        Strategies is a list of each player's strategies (the length of the list
        is equal to the number of players participating to the game. Each strategy
        must contain an individual strategy (triplet) for each round.
        """
        self.players = list()
        totalWealth = np.sum(wealths)
        for index, strategy in enumerate(strategies):
            self.players.append(Player(wealths[index], riskCurve, riskLambda, lossFracts[index], totalWealth))
            self.players[-1].addStrategies(strategy)
        # the list of strategies should contain, for each player, a list
        # of triplets which represent the strategies the player should adopt
        # in each round of the game
        self.roundsNb = roundsNb
        self.riskEventTime = self.initRiskEventTime(riskEventTime)
        self.totalContributionPerRound = list()

    def initRiskEventTime(self, riskEventTime):
        """
        Initialize riskEventTime which represents when the loss risk will occur.
        Parameter riskEventTime:
                NO_ROUND: risk occurs at no round
                RANDOM_ROUND: risk occurs at a random round
                EVERY_ROUND: risk occurs at each round
                0 < (int) r  < roundNb: risk occur at round r
        RETURNS a specific round or EVERY_ROUND
        """
        if riskEventTime == RANDOM_ROUND:
            riskEventTime = np.random.randint(0, roundsNb)
        elif riskEventTime > 0: # get the right index round
            riskEventTime -= 1
        return riskEventTime

    def play(self):
        """
        Play the game for the given number of rounds.
        """
        totalContribution = 0
        for round in range(self.roundsNb) :
            contributions = [0 for i in range(len(self.players))]
            for i, player in enumerate(self.players) :
                contributions[i] = player.computeContribution(totalContribution, round)
                player.registerContribution(contributions[i])
            totalContribution += np.sum(contributions)
            self.totalContributionPerRound.append(totalContribution)
            # for each player, check if it lost
            for player in self.players :
                player.registerLoss(totalContribution, round, self.riskEventTime)

    def getFinalPayoffs(self) :
        """
        Play the game in order to compute each player's payoff and
        returns a list with these payoffs.
        """
        self.play()
        #print("Contributions per round: ", self.totalContributionPerRound)
        return [player.currentWealth for player in self.players]

    def getTotalContributionPerRound(self):
        return self.totalContributionPerRound

class Simulation():
    def __init__(self, generationsNb=10, gamesNb=10, roundsNb=4, populationSize=100, groupSize=2, contribution=RANDOM_CONTRIBUTION, wealth=1, 
                    lossFract=0.5, riskEventTime=EVERY_ROUND, riskAspect=-1, riskLambda=1, mutationProb=0.03, thresholdDev=0.15):
        self.generationsNb = generationsNb
        self.gamesNb = gamesNb
        self.roundsNb = roundsNb
        self.populationSize = populationSize
        self.groupSize = groupSize
        self.wealth = wealth
        self.contribution = contribution
        self.lossFract = lossFract
        self.riskEventTime = riskEventTime
        self.riskAspect = riskAspect
        if riskAspect == 0 :
            self.riskCurve = self.linearLossProba
        elif riskAspect == 1 :
            self.riskCurve = self.powerLossProba
        elif riskAspect == 2 :
            self.riskCurve = self.stepLikeLossProba
        else :
            self.riskCurve = self.noLossProba
        self.riskLambda = riskLambda
        self.mutationProb = mutationProb
        self.thresholdDev = thresholdDev
        self.simsResults = list()

    def reinit(self, generationsNb=10, gamesNb=10, roundsNb=4, populationSize=100, groupSize=2, contribution=RANDOM_CONTRIBUTION, wealth=1, 
                    lossFract=0.5, riskEventTime=EVERY_ROUND, riskAspect=-1, riskLambda=1, mutationProb=0.03, thresholdDev=0.15):
        self.generationsNb = generationsNb
        self.gamesNb = gamesNb
        self.roundsNb = roundsNb
        self.populationSize = populationSize
        self.groupSize = groupSize
        self.wealth = wealth
        self.contribution = contribution
        self.lossFract = lossFract
        self.riskEventTime = riskEventTime
        self.riskAspect = riskAspect
        if riskAspect == 0 :
            self.riskCurve = self.linearLossProba
        elif riskAspect == 1 :
            self.riskCurve = self.powerLossProba
        elif riskAspect == 2 :
            self.riskCurve = self.stepLikeLossProba
        else :
            self.riskCurve = self.noLossProba
        self.riskLambda = riskLambda
        self.mutationProb = mutationProb
        self.thresholdDev = thresholdDev
        # self.simsResults is not reinited in order to save the results when using pooling
        # because the return with pooling is not working 

    def getSimsResults(self):
        return self.simsResults

    def noLossProba(self, totalContribution=0, lmbda=0, initialWealth=0):
        """
        Default risk function (no risk of loosing).
        """
        return 0

    def linearLossProba(self, totalContribution, lmbda, initialWealth):
        """
        Linearly descending risk function.
        """
        return 1 - (totalContribution / initialWealth) * lmbda

    def powerLossProba(self, totalContribution, lmbda, initialWealth):
        """
        Power curve descending risk function.
        """
        return 1 - ((totalContribution / initialWealth) ** lmbda)

    def stepLikeLossProba(self, totalContribution, lmbda, initialWealth):
        """
        Step-like descending risk function.
        """
        return 1 / (1 + np.exp((lmbda) * (totalContribution / initialWealth - 0.5)))

    def initStrategies(self):
        strategies = []
        for i in range(self.populationSize):
            playerStrategies = []
            for j in range(self.roundsNb):
                threshold = afterThreshold = beforeThreshold = 0
                if self.contribution == RANDOM_CONTRIBUTION: # random values
                    if j == 0:
                        threshold = 0
                        afterThreshold = beforeThreshold = np.random.uniform(0, self.wealth)
                    else:
                        threshold = np.random.uniform()
                        beforeThreshold = np.random.uniform(0, self.wealth)
                        afterThreshold = np.random.uniform(0, self.wealth)
                else:
                    if j == 0:
                        threshold = 0
                        afterThreshold = beforeThreshold = self.contribution
                    else:
                        threshold = np.random.uniform()
                        afterThreshold = beforeThreshold = self.contribution
                playerStrategies.append([threshold, afterThreshold, beforeThreshold])
            strategies.append(playerStrategies)
        return strategies

    def simulation(self):
        # the initial population
        population = np.arange(0, self.populationSize, 1)
        # read wealth and strategies
        wealths = np.full((self.populationSize,), self.wealth)
        strategies = self.initStrategies()
        lossFracts = np.full((self.populationSize,), self.lossFract)
        #print("Wealths: ", wealths)
        #print("Strategies per individual: ")
        simContributionAvgPerRound = np.full(self.roundsNb, 0)
        #print("Sim avgs: ",simContributionAvgPerRound)
        #for i in range(self.populationSize): print(strategies[i])
        simResults = SimResults(self.generationsNb, self.gamesNb, self.roundsNb, self.populationSize, self.groupSize, self.contribution, self.wealth, 
                                self.lossFract, self.riskEventTime, self.riskAspect, self.riskLambda, self.mutationProb, self.thresholdDev)
        for generation in range(self.generationsNb):
            averagePayoff = np.zeros(self.populationSize, dtype=float)
            for game in range(self.gamesNb):
                # randomly choose the group of players
                players = np.random.choice(population, self.groupSize, replace=False)
                # create a game with the chosen players
                correspWealths = [wealths[population[i]] for i in players]
                correspStrateg = [strategies[population[i]] for i in players]
                game = Game(self.roundsNb, correspStrateg, correspWealths, lossFracts, self.riskEventTime, self.riskCurve, self.riskLambda)
                payoffs = game.getFinalPayoffs()
                simContributionAvgPerRound = np.add(simContributionAvgPerRound,np.asarray(game.getTotalContributionPerRound()))
                #print("Sim Avg Per Round: ", simContributionAvgPerRound)
                # save the result of the game in averagePayoff
                for i, player in enumerate(players) :
                    averagePayoff[population[player]] += payoffs[i]
            # average the player's payoff in each game he played
            averagePayoff /= self.gamesNb
            # create the next generation from the current population
            fitness = np.exp(averagePayoff)
            population = self.wrightFisher(population,fitness)
            # update the wealth and the strategy to reflect the new population
            newWealth = [wealths[population[i]] for i in range(len(population))]
            wealths = newWealth
            newStrateg = [strategies[population[i]] for i in range(len(strategies))]
            strategies = newStrateg
            # introduce mutations
            for i in range(len(strategies)) :
                for j in range(self.roundsNb):
                    if np.random.uniform(0., 1.) < self.mutationProb :
                        # threshold error
                        strategies[i][j][0] = np.random.normal(strategies[i][j][0], self.thresholdDev)
                    if np.random.uniform(0., 1.) < self.mutationProb :
                        # contribution error
                        strategies[i][j][1] = np.random.uniform(0, wealths[i])
                        strategies[i][j][2] = np.random.uniform(0, wealths[i])
        simContributionAvgPerRound = np.divide(simContributionAvgPerRound, self.generationsNb*self.gamesNb*self.groupSize*self.wealth)
        simResults.setSimContributionAvgPerRound(simContributionAvgPerRound)
        self.simsResults.append(simResults) # Save Results when using pooling
        #print("Final Sim Avg Per Round: ", self.simResults.getSimContributionAvgPerRound())
        #print("Final Sim Avg",self.simResults.getSimContributionAvg())
        return simResults

    def wrightFisher(self, population, fitness):
        """
        The members of the new population will be selected from the old one
        based on their fitness such that the better members reproduce, while
        the worse members are extincted.
        """
        relativeFitness = fitness / np.sum(fitness)
        return np.random.choice(population, p=relativeFitness, size=self.populationSize, replace=True)


if __name__ == "__main__":
    """
    print("Run of a game")
    roundsNb = 2
    strategies = [[[0.0,0.1,0.0],[0.2,0.1,0.5]], [[0.1,0.5,0.1],[0.7,0.2,0.5]]]
    wealths = [1,1]
    lossFracts = [1,1]
    def riskCurve(x, y, z) :
        return 0

    riskEventTime = EVERY_ROUND
    riskLambda = 0

    game = Game(roundsNb, strategies, wealths, lossFracts, riskEventTime, riskCurve, riskLambda)
    print(game.getFinalPayoffs())
    """


    '''
    s = Simulation(0,0,0,0,0)
    fig = plt.figure()
    x = np.arange(0, 1, 0.01)
    plt.plot(x, s.powerLossProba(x, 0.01, 1), label="0.01")
    plt.plot(x, s.powerLossProba(x, 0.1, 1), label="0.1")
    plt.plot(x, s.powerLossProba(x, 1, 1), label="1")
    plt.plot(x, s.powerLossProba(x, 10, 1), label="10")
    plt.plot(x, s.powerLossProba(x, 100, 1), label="100")
    plt.legend()
    plt.show()
    '''


    """
    print("")
    print("Run of simulation")
    generationsNb = 1
    gamesNb = 3
    roundsNb = 2
    populationSize = 5
    groupSize = 2
    contribution = RANDOM_CONTRIBUTION # RANDOM_CONTRIBUTION or a specific contribution for all players and all the rounds
    wealth = 1
    lossFract = 0.5
    riskEventTime = RANDOM_ROUND # NO_ROUND, EVERY_ROUND, RANDOM_ROUND or a specific round

    simsResults = list()
    # Make the loops to do the simulations
    sim = Simulation(generationsNb, gamesNb, roundsNb, populationSize, groupSize, contribution, wealth, lossFract, riskEventTime)
    simsResults.append(sim.simulation())

    outfile = open("sims",'wb')
    pk.dump(simsResults, outfile)
    outfile.close()
    """



    print("")
    print("Run of simulations for Figure 1 and 2")

    # Parameters for Figure 1 and 2
    generationsNb = 1000
    gamesNb = 100
    roundsNbList = [1, 2, 4]
    populationSize = 5
    groupSize = 2
    contribution = RANDOM_CONTRIBUTION # RANDOM_CONTRIBUTION or a specific contribution for all players and all the rounds
    wealth = 1
    lossFractList = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1] 
    riskEventTimeList = [EVERY_ROUND, RANDOM_ROUND, 1, 0] # EVERY_ROUND, RANDOM_ROUND, First round, Last round (updated in loops)
    riskAspectList = [LINEAR, EXPONENTIAL, STEPLIKE]
    riskLambda = 1
    mutationProb = 0.03
    thresholdDev = 0.15

    # Start simulations
    # Execution time for this 1h30
    # At each 0 added to generationsNb or gamesNb the execution time will be mutiplied by 10.
    start_time = time.time()
    pool = Pool(processes=12)
    sim = Simulation()
    for roundsNb in roundsNbList:
        for lossFract in lossFractList:
            riskEventTimeList[-1] = roundsNb # update the last round with the current number of rounds
            for riskEventTime in riskEventTimeList:
                for riskAspect in riskAspectList:
                    simParamsStr = ("GenNb: " + str(generationsNb) + " GamesNb: " + str(gamesNb) + " RoundsNb: " + str(roundsNb) + 
                        " PopSize: " + str(populationSize) + " GroupSize: " + str(groupSize) + " Contribution: " + str(contribution) + 
                        " Wealth: " + str(wealth) + " LossFract: " + str(lossFract) + " RiskEventTime: " + str(riskEventTime) +
                        " RiskAspect: " + str(riskAspect) + " RiskLambda: " + str(riskLambda) + " MutationProb: " + str(mutationProb) + 
                        " ThresholdDev: " + str(thresholdDev))
                    print(simParamsStr)
                    sim.reinit(generationsNb, gamesNb, roundsNb, populationSize, groupSize, contribution, wealth, 
                                lossFract, riskEventTime, riskAspect, riskLambda, mutationProb, thresholdDev)
                    pool.apply_async(sim.simulation())
    pool.close()
    pool.join()
    simsResults = sim.getSimsResults()
    # Save results
    saveObjectToFile(simsResults,"simsFigs12")
    elapsedSeconds = time.time() - start_time
    print("Simulation Elapsed Time: ",convert_secs_to_hms(elapsedSeconds))
