import pickle as pk
import numpy as np

class SimResults():
    def __init__(self, generationsNb, gamesNb, roundsNb, populationSize, groupSize, contribution, wealth, 
                    lossFract, riskEventTime, riskAspect, riskLambda, mutationProb, thresholdDev):
        self.generationsNb = generationsNb
        self.gamesNb = gamesNb
        self.roundsNb = roundsNb
        self.populationSize = populationSize
        self.groupSize = groupSize
        self.contribution = contribution
        self.wealth = wealth
        self.lossFract = lossFract
        self.riskEventTime = riskEventTime
        self.riskAspect = riskAspect
        self.riskLambda = riskLambda
        self.mutationProb = mutationProb
        self.thresholdDev = thresholdDev
        self.simContributionAvgPerRound = list()

    def getGenerationsNb(self):
        return self.generationsNb

    def getGamesNb(self):
        return self.gamesNb
    
    def getRoundsNb(self):
        return self.roundsNb
    
    def getPopulationSize(self):
        return self.populationSize
    
    def getGroupSize(self):
        return self.groupSize
    
    def getContribution(self):
        return self.contribution
    
    def getWealth(self):
        return self.wealth
    
    def getLossFract(self):
        return self.lossFract
    
    def getRiskEventTime(self):
        return self.riskEventTime
    
    def getRiskAspect(self):
        return self.riskAspect
    
    def getRiskLambda(self):
        return self.riskLambda
    
    def getMutationProb(self):
        return self.mutationProb
    
    def getThresholdDev(self):
        return self.thresholdDev

    def setSimContributionAvgPerRound(self, simContributionAvgPerRound):
        self.simContributionAvgPerRound = simContributionAvgPerRound

    def getSimContributionAvgPerRound(self):
        return self.simContributionAvgPerRound

    def getSimContributionAvg(self):
        return np.divide(np.sum(self.simContributionAvgPerRound), self.roundsNb)

    def __repr__(self):
        repr = ("GenNb: " + str(self.generationsNb) + " GamesNb: " + str(self.gamesNb) + " RoundsNb: " + str(self.roundsNb) + 
                " PopSize: " + str(self.populationSize) + " GroupSize: " + str(self.groupSize) + " Contribution: " + str(self.contribution) + 
                " Wealth: " + str(self.wealth) + " LossFract: " + str(self.lossFract) + " RiskEventTime: " + str(self.riskEventTime) +
                " RiskAspect: " + str(self.riskAspect) + " RiskLambda: " + str(self.riskLambda) + " MutationProb: " + str(self.mutationProb) + 
                " ThresholdDev: " + str(self.thresholdDev))
        return repr

def saveObjectToFile(object, filename):
    outfile = open(filename,'wb')
    pk.dump(object, outfile)
    outfile.close()

def loadObjectFromFile(filename):
    infile = open(filename,'rb')
    object = pk.load(infile)
    infile.close()
    return object

def convert_secs_to_hms(seconds):
    minutes = seconds // 60
    hours = minutes // 60
    return ("%02d:%02d:%02d" % (hours, minutes % 60, seconds % 60))