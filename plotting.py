import pickle as pk
import matplotlib.pyplot as plt

from utils import *

LINEAR = 0
EXPONENTIAL = 1
STEPLIKE = 2
ZERO = -1

RANDOM_CONTRIBUTION = -1

NO_ROUND = -1
RANDOM_ROUND = -2
EVERY_ROUND = -3

lossFractList = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
roundsNb = [1 ,2, 4]
riskAspectList = [LINEAR, EXPONENTIAL, STEPLIKE]
riskAspectLabels = ["LINEAR", "EXPONENTIAL", "STEPLIKE"]

def createFig1(simsFigs12): 
	fig = plt.figure(1, figsize=(15, 5))
	fig.subplots_adjust(hspace=.5,wspace=.5)
	plots = [fig.add_subplot(131), fig.add_subplot(132), fig.add_subplot(133)]
	for roundIndex in range(len(roundsNb)):
		for riskAspectIndex in range(len(riskAspectList)):
			contribAvgs = list()
			for sim in simsFigs12:
				if (sim.getRoundsNb() == roundsNb[roundIndex] and sim.getRiskEventTime() == EVERY_ROUND 
					and sim.getRiskAspect() == riskAspectList[riskAspectIndex] and sim.getLossFract() in lossFractList):
						contribAvgs.append(sim.getSimContributionAvg())
			#print(len(lossFractList))
			#print(len(contribAvgs))
			plots[roundIndex].set_xlabel(r'Loss fraction $ \alpha$')
			plots[roundIndex].set_ylabel("Contribution Avg")
			plots[roundIndex].set_title(r'$\Omega$: '+ str(roundsNb[roundIndex]))
			plots[roundIndex].plot(lossFractList, contribAvgs, label=riskAspectLabels[riskAspectIndex])
			plots[roundIndex].scatter(lossFractList, contribAvgs)
	#fig.legend(title = "Risk Curves")
	fig.legend(title = "Risk Curves", loc='upper right')	
	plt.show()

if __name__ == "__main__":
	simsFigs12 = loadObjectFromFile("simsFigs12")
	createFig1(simsFigs12)